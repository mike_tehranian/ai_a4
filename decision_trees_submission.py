from __future__ import division

import numpy as np
from collections import Counter
import time


class DecisionNode:
    """Class to represent a single node in a decision tree."""

    def __init__(self, left, right, decision_function, class_label=None):
        """Create a decision function to select between left and right nodes.

        Note: In this representation 'True' values for a decision take us to
        the left. This is arbitrary but is important for this assignment.

        Args:
            left (DecisionNode): left child node.
            right (DecisionNode): right child node.
            decision_function (func): function to decide left or right node.
            class_label (int): label for leaf node. Default is None.
        """

        self.left = left
        self.right = right
        self.decision_function = decision_function
        self.class_label = class_label

    def decide(self, feature):
        """Get a child node based on the decision function.

        Args:
            feature (list(int)): vector for feature.

        Return:
            Class label if a leaf node, otherwise a child node.
        """

        if self.class_label is not None:
            return self.class_label

        elif self.decision_function(feature):
            return self.left.decide(feature)

        else:
            return self.right.decide(feature)


def load_csv(data_file_path, class_index=-1):
    """Load csv data in a numpy array.

    Args:
        data_file_path (str): path to data file.
        class_index (int): slice output by index.

    Returns:
        features, classes as numpy arrays if class_index is specified,
            otherwise all as nump array.
    """

    handle = open(data_file_path, 'r')
    contents = handle.read()
    handle.close()
    rows = contents.split('\n')
    out = np.array([[float(i) for i in r.split(',')] for r in rows if r])

    if class_index == -1:
        classes = map(int, out[:, class_index])
        features = out[:, :class_index]
        return features, classes

    elif class_index == 0:
        classes = map(int, out[:, class_index])
        features = out[:, 1:]
        return features, classes

    else:
        return out


def build_decision_tree():
    """Create a decision tree capable of handling the provided data.

    Tree is built fully starting from the root.

    Returns:
        The root node of the decision tree.
    """
    decision_tree_root = None

    decision_tree_root = DecisionNode(None, None, lambda x: x[0] == 1)
    decision_tree_root.left = DecisionNode(None, None, None, class_label=1)
    decision_tree_root.right = DecisionNode(None, None, lambda x: x[2] == 1)

    decision_tree_root.right.left = DecisionNode(None, None, lambda x: x[3] == 1)

    decision_tree_root.right.left.left = DecisionNode(None, None, None, class_label=1)
    decision_tree_root.right.left.right = DecisionNode(None, None, None, class_label=0)

    decision_tree_root.right.right = DecisionNode(None, None, lambda x: x[3] == 1)
    decision_tree_root.right.right.left = DecisionNode(None, None, None, class_label=0)
    decision_tree_root.right.right.right = DecisionNode(None, None, None, class_label=1)

    return decision_tree_root


def confusion_matrix(classifier_output, true_labels):
    """Create a confusion matrix to measure classifier performance.

    Output will in the format:
        [[true_positive, false_negative],
         [false_positive, true_negative]]

    Args:
        classifier_output (list(int)): output from classifier.
        true_labels: (list(int): correct classified labels.

    Returns:
        A two dimensional array representing the confusion matrix.
    """
    ground_truth = np.array(true_labels)
    predicted = np.array(classifier_output)

    return [[np.sum(np.logical_and(predicted == 1, ground_truth == 1)),
             np.sum(np.logical_and(predicted == 0, ground_truth == 1))],
            [np.sum(np.logical_and(predicted == 1, ground_truth == 0)),
             np.sum(np.logical_and(predicted == 0,  ground_truth == 0))]]


def precision(classifier_output, true_labels):
    """Get the precision of a classifier compared to the correct values.

    Precision is measured as:
        true_positive/ (true_positive + false_positive)

    Args:
        classifier_output (list(int)): output from classifier.
        true_labels: (list(int): correct classified labels.

    Returns:
        The precision of the classifier output.
    """
    ground_truth = np.array(true_labels)
    predicted = np.array(classifier_output)

    return np.sum(np.logical_and(predicted == 1, ground_truth == 1)) / \
            np.sum(predicted == 1, dtype=np.float64)


def recall(classifier_output, true_labels):
    """Get the recall of a classifier compared to the correct values.

    Recall is measured as:
        true_positive/ (true_positive + false_negative)

    Args:
        classifier_output (list(int)): output from classifier.
        true_labels: (list(int): correct classified labels.

    Returns:
        The recall of the classifier output.
    """
    ground_truth = np.array(true_labels)
    predicted = np.array(classifier_output)

    return np.sum(np.logical_and(predicted == 1, ground_truth == 1)) / \
            np.sum(ground_truth == 1, dtype=np.float64)


def accuracy(classifier_output, true_labels):
    """Get the accuracy of a classifier compared to the correct values.

    Accuracy is measured as:
        correct_classifications / total_number_examples

    Args:
        classifier_output (list(int)): output from classifier.
        true_labels: (list(int): correct classified labels.

    Returns:
        The accuracy of the classifier output.
    """
    ground_truth = np.array(true_labels)
    predicted = np.array(classifier_output)

    return np.sum(predicted == ground_truth, dtype=np.float64) / len(ground_truth)


def gini_impurity(class_vector):
    """Compute the gini impurity for a list of classes.
    This is a measure of how often a randomly chosen element
    drawn from the class_vector would be incorrectly labeled
    if it was randomly labeled according to the distribution
    of the labels in the class_vector.
    It reaches its minimum at zero when all elements of class_vector
    belong to the same class.

    Args:
        class_vector (list(int)): Vector of classes given as 0 or 1.

    Returns:
        Floating point number representing the gini impurity.
    """
    class_values = np.array(class_vector)
    num_values = len(class_values)

    class_0 = np.sum(class_values == 0, dtype=np.float64)
    class_1 = np.sum(class_values == 1, dtype=np.float64)

    if class_0 == 0.0 or class_1 == 0.0:
        # All elements belong to the same class
        return 0.0

    return 1.0 - ((class_0 / num_values)**2 + (class_1 / num_values)**2)


def gini_gain(previous_classes, current_classes):
    """Compute the gini impurity gain between the previous and current classes.
    Args:
        previous_classes (list(int)): Vector of classes given as 0 or 1.
        current_classes (list(list(int): A list of lists where each list has
            0 and 1 values).
    Returns:
        Floating point number representing the information gain.
    """
    previous_gini_impurity = gini_impurity(previous_classes)
    current_gini_impurity = 0.0

    for current_class_item in current_classes:
        current_gini_impurity += (len(current_class_item) / len(previous_classes)) \
                                    * gini_impurity(current_class_item)

    return previous_gini_impurity - current_gini_impurity


class DecisionTree:
    """Class for automatic tree-building and classification."""

    def __init__(self, depth_limit=float('inf')):
        """Create a decision tree with a set depth limit.

        Starts with an empty root.

        Args:
            depth_limit (float): The maximum depth to build the tree.
        """

        self.root = None
        self.depth_limit = depth_limit

    def fit(self, features, classes):
        """Build the tree from root using __build_tree__().

        Args:
            features (list(list(int)): List of features.
            classes (list(int)): Available classes.
        """

        self.root = self.__build_tree__(features, classes)

    def __build_tree__(self, features, classes, depth=0):
        """Build tree that automatically finds the decision functions.

        Args:
            features (list(list(int)): List of features.
            classes (list(int)): Available classes.
            depth (int): max depth of tree.  Default is 0.

        Returns:
            Root node of decision tree.
        """
        classifications = np.array(classes)
        if len(np.unique(classifications)) == 1 or depth == self.depth_limit:
            classification_counts = np.bincount(classifications)
            max_classification = np.argmax(classification_counts)
            return DecisionNode(None, None, None, class_label=max_classification)

        alpha_best = None
        alpha_best_value = None
        alpha_best_gini_gain = float('-inf')
        for alpha in range(features.shape[1]):
            feature_values = features[:, alpha]
            decision_boundary_candidates = np.unique(feature_values)

            if len(decision_boundary_candidates) != 1:
                # Find the half-way linear distance between each of the alpha values
                decision_boundary_candidates = (decision_boundary_candidates[1:]
                                                + decision_boundary_candidates[:-1]) / 2.0
                # Skip every other decision boundary in order to create decision boundaries
                # of at least two data points
                decision_boundary_candidates = decision_boundary_candidates[::2]

            for boundary_value in decision_boundary_candidates:
                true_classes = classifications[feature_values <= boundary_value]
                false_classes = classifications[feature_values > boundary_value]

                gain = gini_gain(classifications, (true_classes, false_classes))
                if gain > alpha_best_gini_gain:
                    alpha_best = alpha
                    alpha_best_value = boundary_value
                    alpha_best_gini_gain = gain

        true_indexes = features[:, alpha_best] <= alpha_best_value
        false_indexes = features[:, alpha_best] > alpha_best_value

        classified_true = classifications[true_indexes]
        classified_false = classifications[false_indexes]
        if len(classified_true) == 0 or len(classified_false) == 0:
            # If all classifications are the same
            # Apply the base case logic here again to avoid infinite recursion
            classification_counts = np.bincount(classifications)
            max_classification = np.argmax(classification_counts)
            return DecisionNode(None, None, None, class_label=max_classification)

        decision_node = DecisionNode(None, None, lambda x: x[alpha_best] <= alpha_best_value)
        decision_node.left = self.__build_tree__(features[true_indexes], classified_true, depth + 1)
        decision_node.right = self.__build_tree__(features[false_indexes], classified_false, depth + 1)

        return decision_node

    def classify(self, features):
        """Use the fitted tree to classify a list of example features.

        Args:
            features (list(list(int)): List of features.

        Return:
            A list of class labels.
        """
        class_labels = []

        for feature in features:
            class_labels.append(self.root.decide(feature))

        return class_labels


def generate_k_folds(dataset, k):
    """Split dataset into folds.

    Randomly split data into k equal subsets.

    Fold is a tuple (training_set, test_set).
    Set is a tuple (examples, classes).

    Args:
        dataset: dataset to be split.
        k (int): number of subsections to create.

    Returns:
        List of folds.
    """
    features, classes = dataset
    classifications = np.array(classes)

    num_elements_fold = int(len(classifications) / k)
    k_folds = []

    shuffled_indices = np.arange(len(classifications))
    # Shuffle all indices in-place
    np.random.shuffle(shuffled_indices)

    shuffled_features = features[shuffled_indices]
    shuffled_classifications = classifications[shuffled_indices]

    for fold_num in range(k):
        # For each fold, take one of the n/k as the test data
        # and the remaining (n - 1/k) as the training data

        # This ensures that a data is mutually exclusive -
        # there is no overlap between the training and test data
        test_first_idx = fold_num * num_elements_fold
        test_second_idx = (fold_num + 1) * num_elements_fold

        # Take (n - 1/k) as the training data
        # Concatenate all data that is not in the test as training data
        train_features = np.concatenate((shuffled_features[:test_first_idx],
                                        shuffled_features[test_second_idx:]))
        train_classifications = np.concatenate((shuffled_classifications[:test_first_idx],
                                        shuffled_classifications[test_second_idx:]))

        # Take n/k as the test data
        test_features = shuffled_features[test_first_idx:test_second_idx]
        test_classifications = shuffled_classifications[test_first_idx:test_second_idx]

        # Train/Test, Features/Classifications
        k_folds.append(((train_features, train_classifications), (test_features, test_classifications)))

    # Fold #, Train/Test, Features/Classifications
    return k_folds


class RandomForest:
    """Random forest classification."""

    def __init__(self, num_trees, depth_limit, example_subsample_rate,
                 attr_subsample_rate):
        """Create a random forest.

         Args:
             num_trees (int): fixed number of trees.
             depth_limit (int): max depth limit of tree.
             example_subsample_rate (float): percentage of example samples.
             attr_subsample_rate (float): percentage of attribute samples.
        """

        self.trees = []
        self.num_trees = num_trees
        self.depth_limit = depth_limit
        self.example_subsample_rate = example_subsample_rate
        self.attr_subsample_rate = attr_subsample_rate

    def fit(self, features, classes):
        """Build a random forest of decision trees using Bootstrap Aggregation.

            features (list(list(int)): List of features.
            classes (list(int)): Available classes.
        """
        classifications = np.array(classes)
        num_examples, num_features = features.shape
        num_example_subsample = int(num_examples * self.example_subsample_rate)
        num_attr_subsample = int(num_features * self.attr_subsample_rate)

        for _ in range(self.num_trees):
            decision_tree = DecisionTree(self.depth_limit)

            # Subsample the examples with replacement
            example_idxs = np.random.choice(range(num_examples), size=num_example_subsample, replace=True)
            # Subsample the features without replacement
            feature_idxs = np.random.choice(range(num_features), size=num_attr_subsample, replace=False)

            all_features = features[example_idxs]
            selected_features = all_features[:, feature_idxs]
            selected_classifications = classifications[example_idxs]

            decision_tree.fit(selected_features, selected_classifications)

            self.trees.append((decision_tree, feature_idxs))

    def classify(self, features):
        """Classify a list of features based on the trained random forest.

        Args:
            features (list(list(int)): List of features.
        """
        classes = []
        num_examples, num_features = features.shape[:2]

        for tree, feature_idxs in self.trees:
            selected_features = features[:, feature_idxs]
            classes.append(tree.classify(selected_features))

        stacked_results = np.column_stack(classes)
        final_decision = []
        for row in range(num_examples):
            final_decision.append(np.argmax(np.bincount(stacked_results[row,:])))

        return final_decision


class ChallengeClassifier:
    """Challenge Classifier used on Challenge Training Data."""

    def __init__(self):
        """Create challenge classifier.

        Initialize whatever parameters you may need here.
        This method will be called without parameters, therefore provide
        defaults.
        """
        num_trees = 10
        depth_limit = float('inf')
        example_subsample_rate = 0.95
        attr_subsample_rate = 0.65
        self.random_forest = RandomForest(num_trees, depth_limit, example_subsample_rate, attr_subsample_rate)

    def fit(self, features, classes):
        """Build the underlying tree(s).

            Fit your model to the provided features.

        Args:
            features (list(list(int)): List of features.
            classes (list(int)): Available classes.
        """
        self.random_forest.fit(features, classes)

    def classify(self, features):
        """Classify a list of features.

        Classify each feature in features as either 0 or 1.

        Args:
            features (list(list(int)): List of features.

        Returns:
            A list of class labels.
        """
        return self.random_forest.classify(features)


class Vectorization:
    """Vectorization preparation for Assignment 5."""

    def __init__(self):
        pass

    def non_vectorized_loops(self, data):
        """Element wise array arithmetic with loops.

        This function takes one matrix, multiplies by itself and then adds to
        itself.

        Args:
            data: data to be added to array.

        Returns:
            Numpy array of data.
        """

        non_vectorized = np.zeros(data.shape)
        for row in range(data.shape[0]):
            for col in range(data.shape[1]):
                non_vectorized[row][col] = (data[row][col] * data[row][col] +
                                            data[row][col])
        return non_vectorized

    def vectorized_loops(self, data):
        """Element wise array arithmetic using vectorization.

        This function takes one matrix, multiplies by itself and then adds to
        itself.

        Bonnie time to beat: 0.09 seconds.

        Args:
            data: data to be sliced and summed.

        Returns:
            Numpy array of data.
        """
        return data * data + data


    def non_vectorized_slice(self, data):
        """Find row with max sum using loops.

        This function searches through the first 100 rows, looking for the row
        with the max sum. (ie, add all the values in that row together).

        Args:
            data: data to be added to array.

        Returns:
            Tuple (Max row sum, index of row with max sum)
        """

        max_sum = 0
        max_sum_index = 0
        for row in range(100):
            temp_sum = 0
            for col in range(data.shape[1]):
                temp_sum += data[row][col]

            if temp_sum > max_sum:
                max_sum = temp_sum
                max_sum_index = row

        return max_sum, max_sum_index

    def vectorized_slice(self, data):
        """Find row with max sum using vectorization.

        This function searches through the first 100 rows, looking for the row
        with the max sum. (ie, add all the values in that row together).

        Bonnie time to beat: 0.07 seconds

        Args:
            data: data to be sliced and summed.

        Returns:
            Tuple (Max row sum, index of row with max sum)
        """
        row_sums = np.sum(data[:100], axis=1)
        max_row_idx = np.argmax(row_sums)

        return row_sums[max_row_idx], max_row_idx

    def non_vectorized_flatten(self, data):
        """Display occurrences of positive numbers using loops.

         Flattens down data into a 1d array, then creates a dictionary of how
         often a positive number appears in the data and displays that value.

         ie, [(1203,3)] = integer 1203 appeared 3 times in data.

         Args:
            data: data to be added to array.

        Returns:
            List of occurrences [(integer, number of occurrences), ...]
        """

        unique_dict = {}
        flattened = np.hstack(data)
        for item in range(len(flattened)):
            if flattened[item] > 0:
                if flattened[item] in unique_dict:
                    unique_dict[flattened[item]] += 1
                else:
                    unique_dict[flattened[item]] = 1

        return unique_dict.items()

    def vectorized_flatten(self, data):
        """Display occurrences of positive numbers using vectorization.

         Flattens down data into a 1d array, then creates a dictionary of how
         often a positive number appears in the data and displays that value.

         ie, [(1203,3)] = integer 1203 appeared 3 times in data.

         Bonnie time to beat: 15 seconds

         Args:
            data: data to be added to array.

        Returns:
            List of occurrences [(integer, number of occurrences), ...]
        """
        nums_and_counts = np.unique(data[data > 0], return_counts=True)
        return np.column_stack(nums_and_counts)


def return_your_name():
    # return your name
    return "Michael Tehranian"
